import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';

import CourseService from '../../service/CourseService';
import CourseTable from './Table';
import CourseCreate from './CreateForm';
import CourseUpdate from './UpdateForm';
import Sidebar from '../Sidebar';

class Course extends Component {

    constructor(props) {
        super(props);

        this.state = {courses: []}
        this.courseService = new CourseService();

        this.createCourse = this.createCourse.bind(this);
        this.updateCourse = this.updateCourse.bind(this);
        this.deleteCourse = this.deleteCourse.bind(this);
    }

    createCourse(course) {
        this.courseService.createCourse(course)
        .then(res => {
            this.adjustStateOnCreate(res.data);
            this.props.history.push('/course');
        });
    }

    updateCourse(course) {
        this.courseService.updateCourse(course)
        .then(res => {
            this.adjustStateOnUpdate(res.data);
            this.props.history.push('/course');
        });
    }

    deleteCourse(course) {
        if (window.confirm('Are you sure you want to delete this course?')) {
            this.courseService.deleteCourse(course)
            .then(res => {
                this.adjustStateOnDelete(res.data);
            });
        }
    }

    adjustStateOnCreate(course) {
        let adjustedCourses = this.state.courses;
        adjustedCourses.push(course);
        this.setState({courses: adjustedCourses});
    }

    adjustStateOnUpdate(course) {
        let courses = this.state.courses;
        let adjustedCourses = courses.map(c => c._id === course._id ? course : c);
        this.setState({courses: adjustedCourses});
    }

    adjustStateOnDelete(course) {
        let courses = this.state.courses;
        let adjustedCourses = courses.filter(c => c._id !== course._id);
        this.setState({courses: adjustedCourses});
    }

    componentDidMount() {
        this.courseService.getAllCourses()
        .then(res => {
            this.setState({courses: res.data});
        })
        .catch(error => console.log(error));
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="d-flex justify-content-start">
                    <Sidebar>
                        <button className="btn btn-dark"><Link to={"/course/create"} className="nav-link">New Course</Link></button>
                    </Sidebar>

                    <Route exact path='/course' render={props => (
                        <CourseTable {...props} courses={this.state.courses} deleteCourse={this.deleteCourse} />
                    )} />


                    <Route exact path='/course/create' render={props => (
                        <CourseCreate {...props} createCourse={this.createCourse} />
                    )} />

                    <Route exact path='/course/update/:id' render={props => (
                        <CourseUpdate {...props} updateCourse={this.updateCourse} />
                    )} />
                </div>
            </div>
        );
    }
}

export default Course;