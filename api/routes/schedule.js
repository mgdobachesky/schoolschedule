const express = require('express');
const router = express.Router();

const scheduleController = require('../controllers/schedule');

router.get('/all/:studentId', scheduleController.readAll);
router.get('/:scheduleId', scheduleController.read);
router.post('/', scheduleController.create);
router.put('/', scheduleController.update);
router.delete('/', scheduleController.delete);

router.all('*', (req, res) => {
    res.status(404);
    res.json({"error": "Route not found."})
});

module.exports = router;