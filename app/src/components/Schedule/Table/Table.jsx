import React from 'react';
import { Link } from 'react-router-dom';

function ScheduleInfo(props) {
    return(
        <tr>
            <th>{props.schedule.quarter + " Quarter"}</th>
            <th>
                <Link to={props.url + "/update/" + props.schedule._id} className="btn btn-primary">Edit</Link>
            </th>
            <th>
                <button className="btn btn-danger" onClick={() => props.deleteSchedule(props.schedule)}>Delete</button>
            </th>
        </tr>
    );
}

function ScheduleCourse(props) {
    return props.schedule.courses.map((course, index) => {
        return(
            <tr key={course._id}>
                <td colSpan="3">{course.name}</td>
            </tr>
        );
    });
}

function ScheduleTable(props) {
    return props.schedules.map((schedule, index) => {
        return(
            <table key={schedule._id} className="table table-striped">
                <thead className="thead-dark">
                    <ScheduleInfo url={props.url} schedule={schedule} deleteSchedule={props.deleteSchedule} />
                </thead>
                <tbody>
                    <ScheduleCourse schedule={schedule} />
                </tbody>
            </table>
        );
    });
}

let ScheduleTableJSX = function() {
    return (
        <div className="m-2 flex-grow-1">
            <ScheduleTable url={this.props.match.url} schedules={this.props.schedules} deleteSchedule={this.props.deleteSchedule} />
        </div>
    )
}

export default ScheduleTableJSX;