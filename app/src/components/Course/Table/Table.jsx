import React from 'react';
import { Link } from 'react-router-dom';

function CourseRow(props) {
    return props.courses.map((course, index) => {
        return(
            <tr key={course._id}>
                <td>{course.name}</td>
                <td>
                    <Link to={"/course/update/"+course._id} className="btn btn-primary">Edit</Link>
                </td>
                <td>
                    <button className="btn btn-danger" onClick={() => props.deleteCourse(course)}>Delete</button>
                </td>
            </tr>
        );
    });
}

let CourseTableJSX = function() {
    return (
        <div className="m-2 flex-grow-1">
            <table className="table table-striped">
                <thead className="thead-dark">
                    <tr>
                        <th colSpan="3">Courses</th>
                    </tr>
                </thead>
                <tbody>
                    <CourseRow courses={this.props.courses} deleteCourse={this.props.deleteCourse} />
                </tbody>
            </table>
        </div>
    )
}

export default CourseTableJSX;