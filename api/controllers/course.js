const courseService = require('../services/course');

function sendJsonResponse(res, status, content) {
    res.status(status);
    res.json(content);
}

async function createCourse(req, res) {
    try {
        let results = await courseService.create(req.body);
        sendJsonResponse(res, 200, results);
    } catch (error) {
        sendJsonResponse(res, 404, error);
    }
}

async function readCourse(req, res) {
    try {
        let results = await courseService.read(req.params.courseId);
        sendJsonResponse(res, 200, results);
    } catch (error) {
        sendJsonResponse(res, 404, error);
    }
}

async function readAllCourses(req, res) {
    try {
        let results = await courseService.readAll();
        sendJsonResponse(res, 200, results);
    } catch (error) {
        sendJsonResponse(res, 404, error);
    }
}

async function updateCourse(req, res) {
    try {
        let results = await courseService.update(req.body);
        sendJsonResponse(res, 200, results);
    } catch (error) {
        sendJsonResponse(res, 404, error);
    }
}

async function deleteCourse(req, res) {
    try {
        let results = await courseService.delete(req.body);
        sendJsonResponse(res, 200, results);
    } catch (error) {
        sendJsonResponse(res, 404, error);
    }
}

module.exports = {
    create: createCourse,
    read: readCourse,
    readAll: readAllCourses,
    update: updateCourse,
    delete: deleteCourse
}