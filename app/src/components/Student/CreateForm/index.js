import { Component } from 'react';

import StudentCreateJSX from './CreateForm.jsx';
import './CreateForm.css';

class StudentCreate extends Component {

    constructor(props) {
        super(props);
        this.state = {name: ''}

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({[name]: value});
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.createStudent(this.state);
    }

    render() {
        return (StudentCreateJSX.call(this));
    }
}

export default StudentCreate;