const mongoose = require('mongoose');
const dbURI = 'mongodb://localhost/class_schedule';

const gracefulShutdown = function (message, callback) {
    mongoose.connection.close(function () {
        console.log("Mongoose disconnected through " + message);
        callback();
    });
};

mongoose.connect(dbURI);

mongoose.connection.on('connected', function () {
    console.log("Mongoose connected to " + dbURI);
});

mongoose.connection.on('error', function (error) {
    console.log("Mongoose connection error: " + error);
    process.exit(0);
});

mongoose.connection.on('disconnected', function () {
    console.log("Mongoose disconnected");
});

process.on('SIGINT', function () {
    gracefulShutdown('app termination', function () {
        process.exit(0);
    });
});

process.on('exit', function (code) {
    console.log('About to exit with code:', code);
});

require('./schemas/course');
require('./schemas/schedule');
require('./schemas/student');
