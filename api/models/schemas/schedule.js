const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const scheduleSchema = new Schema({
    'student': {
        type: ObjectId,
        ref: 'student'
    },
    'quarter': {
        'type': String,
        'required': true
    },
    'courses': [{
        type: ObjectId,
        ref: 'course'
    }]
});

const schedule = mongoose.model('schedule', scheduleSchema);

module.exports = schedule;