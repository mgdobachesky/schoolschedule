import { Component } from 'react';

import HeaderJSX from './Header.jsx';
import './Header.css';

class Header extends Component {
    render() {
        return (HeaderJSX.call(this));
    }
}

export default Header;