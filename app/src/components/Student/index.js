import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';

import StudentService from '../../service/StudentService'
import StudentCreate from './CreateForm';
import StudentUpdate from './UpdateForm';
import StudentTable from './Table';
import Sidebar from '../Sidebar';

class Student extends Component {

    constructor(props) {
        super(props);

        this.state = {students: []}
        this.studentService = new StudentService();

        this.createStudent = this.createStudent.bind(this);
        this.updateStudent = this.updateStudent.bind(this);
        this.deleteStudent = this.deleteStudent.bind(this);
    }

    createStudent(student) {
        this.studentService.createStudent(student)
        .then(res => {
            this.adjustStateOnCreate(res.data);
            this.props.history.push('/student');
        });
    }

    updateStudent(student) {
        this.studentService.updateStudent(student)
        .then(res => {
            this.adjustStateOnUpdate(res.data);
            this.props.history.push('/student');
        });
    }

    deleteStudent(student) {
        if (window.confirm('Are you sure you want to delete this student?')) {
            this.studentService.deleteStudent(student)
            .then(res => {
                this.adjustStateOnDelete(res.data);
            });
        }
    }

    adjustStateOnCreate(student) {
        let adjustedStudents = this.state.students;
        adjustedStudents.push(student);
        this.setState({students: adjustedStudents});
    }

    adjustStateOnUpdate(student) {
        let students = this.state.students;
        let adjustedStudents = students.map(s => s._id === student._id ? student : s);
        this.setState({students: adjustedStudents});
    }

    adjustStateOnDelete(student) {
        let students = this.state.students;
        let adjustedStudents = students.filter(s => s._id !== student._id);
        this.setState({students: adjustedStudents});
    }

    componentDidMount() {
        this.studentService.getAllStudents()
        .then(res => {
            this.setState({students: res.data});
        })
        .catch(error => console.log(error));
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="d-flex justify-content-start">
                    <Sidebar>
                        <button className="btn btn-dark"><Link to={"/student/create"} className="nav-link">New Student</Link></button>
                    </Sidebar>

                    <Route exact path='/student' render={props => (
                        <StudentTable {...props} students={this.state.students} deleteStudent={this.deleteStudent} />
                    )} />

                    <Route exact path='/student/create' render={props => (
                        <StudentCreate {...props} createStudent={this.createStudent} />
                    )} />

                    <Route exact path='/student/update/:id' render={props => (
                        <StudentUpdate {...props} updateStudent={this.updateStudent} />
                    )} />
                </div>
            </div>
        );
    }
}

export default Student;