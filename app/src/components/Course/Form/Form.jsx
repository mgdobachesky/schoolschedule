import React from 'react';

let CourseFormJSX = function() {
    return (
        <div className="m-2">
            <h2>{this.props.description}</h2>
            <form onSubmit={this.handleSubmit}>
                <label>Name: </label>
                <input type="text"
                    name="name"
                    value={this.props.course.name}
                    onChange={this.handleChange} />
                <input type="submit"
                    value="Submit"
                    className="btn btn-primary" />
            </form>
        </div>
    )
}

export default CourseFormJSX;