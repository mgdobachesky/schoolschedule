import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Student from './components/Student';
import Schedule from './components/Schedule';
import Course from './components/Course';
import Header from './components/Header';
import './index.css';

ReactDOM.render(
    <Router>
        <div>
            <Header />
            <Route path='/student' component={Student} />
            <Route path='/schedule/:studentId' component={Schedule} />
            <Route path='/course' component={Course} />
        </div>
    </Router>,
    document.getElementById('root')
);