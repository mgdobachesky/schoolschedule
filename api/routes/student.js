const express = require('express');
const router = express.Router();

const studentController = require('../controllers/student');

router.get('/all', studentController.readAll);
router.get('/:studentId', studentController.read);
router.post('/', studentController.create);
router.put('/', studentController.update);
router.delete('/', studentController.delete);

router.all('*', (req, res) => {
    res.status(404);
    res.json({"error": "Route not found."})
});

module.exports = router;