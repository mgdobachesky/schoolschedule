import { Component } from 'react';

import ScheduleFormJSX from './Form.jsx';
import './Form.css';

import '../../../service/CourseService';
import CourseService from '../../../service/CourseService';

class ScheduleForm extends Component {

    constructor(props) {
        super(props);

        this.state = {availableCourses: []}

        this.courseService = new CourseService();
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.getCourseIds = this.getCourseIds.bind(this);
    }

    getCourseIds(courses) {
        if(courses) {
            return courses.map(c => c._id);
        }
        return [];
    }

    handleChange(event) {
        this.props.onChange(event);
    }

    handleSubmit(event) {
        this.props.onSubmit(event);
    }

    componentDidMount() {
        this.courseService.getAllCourses()
        .then(res => {
            this.setState({availableCourses: res.data});
        })
        .catch(error => console.log(error));
    }

    render() {
        return (ScheduleFormJSX.call(this));
    }
}

export default ScheduleForm;