import React from 'react';

let SidebarJSX = function() {
    return (
        <div className="m-2">
            {this.props.children}
        </div>
    )
}

export default SidebarJSX;