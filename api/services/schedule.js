const mongoose = require('mongoose');
const scheduleDao = mongoose.model('schedule')

const studentService = require('./student');

function createSchedule(schedule) {
    return new Promise((resolve, reject) => {
        scheduleDao.create({
            'quarter': schedule.quarter,
            'student': schedule.student,
            'courses': schedule.courses
        },
        async (error, results) => {
            if (error) {
                reject('Schedule not created.');
            }
            await results.populate('courses').execPopulate();
            resolve(results);
        });
    });
}

function readSchedule(scheduleId) {
    return new Promise((resolve, reject) => {
        scheduleDao.findById(scheduleId)
        .populate('student')
        .populate('courses')
        .exec((error, results) => {
            if (error) {
                reject('Something went wrong.');
            }
            if (!results) {
                reject('Schedule not found.');
            }
            resolve(results);
        });
    });
}

function readAllSchedules(studentId) {
    return new Promise((resolve, reject) => {
        scheduleDao.find({student: studentId})
        .populate('student')
        .populate('courses')
        .exec((error, results) => {
            if (error) {
                reject('Something went wrong.');
            }
            if (!results) {
                reject('Schedules not found.');
            }
            resolve(results);
        });
    });
}

function updateSchedule(schedule) {
    return new Promise(async (resolve, reject) => {
        try {
            let results = await readSchedule(schedule.id);
            results.quarter = schedule.quarter;
            results.courses = schedule.courses;
            results.save(async (error, updatedResults) => {
                if (error) {
                    reject('Something went wrong.');
                }
                await updatedResults.populate('courses').execPopulate();
                resolve(updatedResults);
            });
        } catch (error) {
            reject(error);
        }
    });
}

function deleteSchedule(schedule) {
    return new Promise((resolve, reject) => {
        scheduleDao.findByIdAndRemove(schedule.id)
        .exec(async (error, results) => {
            if (error) {
                reject('Something went wrong.');
            }
            if (!results) {
                reject('Schedule not found.');
            }
            await results.populate('courses').execPopulate();
            resolve(results);
        });
    });
}

module.exports = {
    create: createSchedule,
    read: readSchedule,
    readAll: readAllSchedules,
    update: updateSchedule,
    delete: deleteSchedule
}