import { Component } from 'react';

import ScheduleTableJSX from './Table.jsx';
import './Table.css';

class ScheduleTable extends Component {
    render() {
        return (ScheduleTableJSX.call(this));
    }
}

export default ScheduleTable;