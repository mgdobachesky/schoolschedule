const mongoose = require('mongoose');
const courseDao = mongoose.model('course')

function createCourse(course) {
    return new Promise((resolve, reject) => {
        courseDao.create({'name': course.name},
        (error, results) => {
            if (error) {
                reject('Course not created.');
            }
            resolve(results);
        });
    });
}

function readCourse(courseId) {
    return new Promise((resolve, reject) => {
        courseDao.findById(courseId)
        .exec((error, results) => {
            if (error) {
                reject('Something went wrong.');
            }
            if (!results) {
                reject('Course not found.');
            }
            resolve(results);
        });
    });
}

function readAllCourses() {
    return new Promise((resolve, reject) => {
        courseDao.find()
        .exec((error, results) => {
            if (error) {
                reject('Something went wrong.');
            }
            if (!results) {
                reject('Courses not found.');
            }
            resolve(results);
        });
    });
}

function updateCourse(course) {
    return new Promise(async (resolve, reject) => {
        try {
            let results = await readCourse(course.id);
            results.name = course.name;
            results.save((error, updatedResults) => {
                if (error) {
                    reject('Something went wrong.');
                }
                resolve(updatedResults);
            });
        } catch (error) {
            reject(error);
        }
    });
}

function deleteCourse(course) {
    return new Promise((resolve, reject) => {
        courseDao.findByIdAndRemove(course.id)
        .exec((error, results) => {
            if (error) {
                reject('Something went wrong.');
            }
            if (!results) {
                reject('Course not found.');
            }
            resolve(results);
        });
    });
}

module.exports = {
    create: createCourse,
    read: readCourse,
    readAll: readAllCourses,
    update: updateCourse,
    delete: deleteCourse
}