import { Component } from 'react';

import CourseCreateJSX from './CreateForm.jsx';
import './CreateForm.css';

class CourseCreate extends Component {

    constructor(props) {
        super(props);
        this.state = {name: ''}

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({[name]: value});
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.createCourse(this.state);
    }

    render() {
        return (CourseCreateJSX.call(this));
    }
}

export default CourseCreate;