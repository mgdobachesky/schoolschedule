const mongoose = require('mongoose');
const studentDao = mongoose.model('student')

function createStudent(student) {
    return new Promise((resolve, reject) => {
        studentDao.create({'name': student.name},
        (error, results) => {
            if (error) {
                reject('Student not created.');
            }
            resolve(results);
        });
    });
}

function readStudent(studentId) {
    return new Promise((resolve, reject) => {
        studentDao.findById(studentId)
        .exec((error, results) => {
            if (error) {
                reject('Something went wrong.');
            }
            if (!results) {
                reject('Student not found.');
            }
            resolve(results);
        });
    });
}

function readAllStudents() {
    return new Promise((resolve, reject) => {
        studentDao.find()
        .exec((error, results) => {
            if (error) {
                reject('Something went wrong.');
            }
            if (!results) {
                reject('Students not found.');
            }
            resolve(results);
        });
    });
}

function updateStudent(student) {
    return new Promise(async (resolve, reject) => {
        let results = await readStudent(student.id);
        results.name = student.name;
        results.save((error, updatedResults) => {
            if (error) {
                reject('Something went wrong.');
            }
            resolve(updatedResults);
        });
    });
}

function deleteStudent(student) {
    return new Promise((resolve, reject) => {
        studentDao.findByIdAndRemove(student.id)
        .exec((error, results) => {
            if (error) {
                reject('Something went wrong.');
            }
            if (!results) {
                reject('Student not found.');
            }
            resolve(results);
        });
    });
}

module.exports = {
    create: createStudent,
    read: readStudent,
    readAll: readAllStudents,
    update: updateStudent,
    delete: deleteStudent
}