import { Component } from 'react';

import CourseFormJSX from './Form.jsx';
import './Form.css';

class CourseForm extends Component {

    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.props.onChange(event);
    }

    handleSubmit(event) {
        this.props.onSubmit(event);
    }

    render() {
        return (CourseFormJSX.call(this));
    }
}

export default CourseForm;