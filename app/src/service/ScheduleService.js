import axios from 'axios';

class ScheduleService {

    constructor() {
        this.baseURL = 'http://localhost:3001';
    }

    getAllSchedules(req) {
        return axios({
            url: '/schedule/all/' + req.studentId,
            method: 'get',
            baseURL: this.baseURL
        });
    }

    getSchedule(req) {
        return axios({
            url: '/schedule/' + req.id,
            method: 'get',
            baseURL: this.baseURL
        });
    }

    createSchedule(req) {
        return axios({
            url: '/schedule',
            method: 'post',
            baseURL: this.baseURL,
            headers: {'Content-Type': 'application/json'},
            data: {
                'quarter': req.quarter,
                'courses': req.courses,
                'student': req.student
            }
        });
    }

    updateSchedule(req) {
        return axios({
            url: '/schedule',
            method: 'put',
            baseURL: this.baseURL,
            headers: {'Content-Type': 'application/json'},
            data: {
                'id': req._id,
                'quarter': req.quarter,
                'courses': req.courses
            }
        });
    }

    deleteSchedule(req) {
        return axios({
            url: '/schedule',
            method: 'delete',
            baseURL: this.baseURL,
            headers: {'Content-Type': 'application/json'},
            data: {
                'id': req._id
            }
        });
    }

}

export default ScheduleService;