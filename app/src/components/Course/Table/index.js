import { Component } from 'react';

import CourseTableJSX from './Table.jsx';
import './Table.css';

class CourseTable extends Component {
    render() {
        return (CourseTableJSX.call(this));
    }
}

export default CourseTable;