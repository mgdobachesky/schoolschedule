import axios from 'axios';

class StudentService {

    constructor() {
        this.baseURL = 'http://localhost:3001';
    }

    getAllStudents(req) {
        return axios({
            url: '/student/all',
            method: 'get',
            baseURL: this.baseURL
        });
    }

    getStudent(req) {
        return axios({
            url: '/student/' + req.id,
            method: 'get',
            baseURL: this.baseURL
        });
    }

    createStudent(req) {
        return axios({
            url: '/student',
            method: 'post',
            baseURL: this.baseURL,
            headers: {'Content-Type': 'application/json'},
            data: {
                'name': req.name
            }
        });
    }

    updateStudent(req) {
        return axios({
            url: '/student',
            method: 'put',
            baseURL: this.baseURL,
            headers: {'Content-Type': 'application/json'},
            data: {
                'id': req._id,
                'name': req.name
            }
        });
    }

    deleteStudent(req) {
        return axios({
            url: '/student',
            method: 'delete',
            baseURL: this.baseURL,
            headers: {'Content-Type': 'application/json'},
            data: {
                'id': req._id
            }
        });
    }

}

export default StudentService;