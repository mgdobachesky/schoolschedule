import { Component } from 'react';

import StudentService from '../../../service/StudentService';
import StudentUpdateJSX from './UpdateForm.jsx';
import './UpdateForm.css';

class StudentUpdate extends Component {

    constructor(props) {
        super(props);
        this.state = {name: ''}

        this.studentService = new StudentService();

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.studentService.getStudent(this.props.match.params)
        .then(res => {
            this.setState(res.data);
        }).catch(err => console.log(err));
    }

    handleChange(event) {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({[name]: value});
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.updateStudent(this.state);
    }

    render() {
        return (StudentUpdateJSX.call(this));
    }
}

export default StudentUpdate;