import axios from 'axios';

class CourseService {

    constructor() {
        this.baseURL = 'http://localhost:3001';
    }

    getAllCourses(req) {
        return axios({
            url: '/course/all',
            method: 'get',
            baseURL: this.baseURL
        });
    }

    getCourse(req) {
        return axios({
            url: '/course/' + req.id,
            method: 'get',
            baseURL: this.baseURL
        });
    }

    createCourse(req) {
        return axios({
            url: '/course',
            method: 'post',
            baseURL: this.baseURL,
            headers: {'Content-Type': 'application/json'},
            data: {
                'name': req.name
            }
        });
    }

    updateCourse(req) {
        return axios({
            url: '/course',
            method: 'put',
            baseURL: this.baseURL,
            headers: {'Content-Type': 'application/json'},
            data: {
                'id': req._id,
                'name': req.name
            }
        });
    }

    deleteCourse(req) {
        return axios({
            url: '/course',
            method: 'delete',
            baseURL: this.baseURL,
            headers: {'Content-Type': 'application/json'},
            data: {
                'id': req._id
            }
        });
    }

}

export default CourseService;