import React from 'react';

function CourseOptions(props) {
    return props.availableCourses.map((course, index) => {
        return(
            <option key={course._id} value={course._id}>{course.name}</option>
        );
    });
}


let ScheduleFormJSX = function() {
    return (
        <div className="m-2">
            <h2>{this.props.description}</h2>
            <form onSubmit={this.handleSubmit}>
                <label>Quarter: </label>
                <input type="text"
                    name="quarter"
                    value={this.props.schedule.quarter}
                    onChange={this.handleChange} />
                <div className="input-group">
                    <label>Courses: </label>
                    <select className="custom-select"
                        multiple={true}
                        name="courses"
                        value={this.getCourseIds(this.props.schedule.courses)}
                        onChange={this.handleChange}>
                        <CourseOptions availableCourses={this.state.availableCourses} />
                    </select>
                </div>
                <input type="submit"
                    value="Submit"
                    className="btn btn-primary" />
            </form>
        </div>
    )
}

export default ScheduleFormJSX;