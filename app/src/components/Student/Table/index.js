import { Component } from 'react';

import StudentTableJSX from './Table.jsx';
import './Table.css';

class StudentTable extends Component {
    render() {
        return (StudentTableJSX.call(this));
    }
}

export default StudentTable;