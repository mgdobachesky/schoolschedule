import { Component } from 'react';

import SidebarJSX from './Sidebar.jsx';
import './Sidebar.css';

class Sidebar extends Component {
    render() {
        return (SidebarJSX.call(this));
    }
}

export default Sidebar;