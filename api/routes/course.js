const express = require('express');
const router = express.Router();

const courseController = require('../controllers/course');

router.get('/all', courseController.readAll);
router.get('/:courseId', courseController.read);
router.post('/', courseController.create);
router.put('/', courseController.update);
router.delete('/', courseController.delete);

router.all('*', (req, res) => {
    res.status(404);
    res.json({"error": "Route not found."})
});

module.exports = router;