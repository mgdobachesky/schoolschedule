import { Component } from 'react';

import StudentFormJSX from './Form.jsx';
import './Form.css';

class StudentForm extends Component {

    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.props.onChange(event);
    }

    handleSubmit(event) {
        this.props.onSubmit(event);
    }

    render() {
        return (StudentFormJSX.call(this));
    }
}

export default StudentForm;