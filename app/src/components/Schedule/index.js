import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';

import ScheduleService from '../../service/ScheduleService';
import ScheduleTable from './Table';
import ScheduleCreate from './CreateForm';
import ScheduleUpdate from './UpdateForm';
import Sidebar from '../Sidebar';

class Schedule extends Component {

    constructor(props) {
        super(props);

        this.state = {schedules: []}
        this.scheduleService = new ScheduleService();

        this.createSchedule = this.createSchedule.bind(this);
        this.updateSchedule = this.updateSchedule.bind(this);
        this.deleteSchedule = this.deleteSchedule.bind(this);
    }

    createSchedule(schedule) {
        this.scheduleService.createSchedule(schedule)
        .then(res => {
            this.adjustStateOnCreate(res.data);
            this.props.history.push('/schedule/' + this.props.match.params.studentId);
        });
    }

    updateSchedule(schedule) {
        this.scheduleService.updateSchedule(schedule)
        .then(res => {
            this.adjustStateOnUpdate(res.data);
            this.props.history.push('/schedule/' + this.props.match.params.studentId);
        });
    }

    deleteSchedule(schedule) {
        if (window.confirm('Are you sure you want to delete this schedule?')) {
            this.scheduleService.deleteSchedule(schedule)
            .then(res => {
                this.adjustStateOnDelete(res.data);
            });
        }
    }

    adjustStateOnCreate(schedule) {
        let adjustedSchedules = this.state.schedules;
        adjustedSchedules.push(schedule);
        this.setState({schedules: adjustedSchedules});
    }

    adjustStateOnUpdate(schedule) {
        let schedules = this.state.schedules;
        let adjustedSchedules = schedules.map(s => s._id === schedule._id ? schedule : s);
        this.setState({schedules: adjustedSchedules});
    }

    adjustStateOnDelete(schedule) {
        let schedules = this.state.schedules;
        let adjustedSchedules = schedules.filter(s => s._id !== schedule._id);
        this.setState({schedules: adjustedSchedules});
    }

    componentDidMount() {
        let student = {studentId: this.props.match.params.studentId}
        this.scheduleService.getAllSchedules(student)
        .then(res => {
            this.setState({schedules: res.data});
        })
        .catch(error => console.log(error));
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="d-flex justify-content-start">
                    <Sidebar>
                        <button className="btn btn-dark"><Link to={"/schedule/" + this.props.match.params.studentId + "/create"} className="nav-link">New Schedule</Link></button>
                    </Sidebar>

                    <Route exact path='/schedule/:studentId' render={props => (
                        <ScheduleTable {...props} schedules={this.state.schedules} deleteSchedule={this.deleteSchedule} />
                    )} />

                    <Route exact path='/schedule/:studentId/create' render={props => (
                        <ScheduleCreate {...props} createSchedule={this.createSchedule} />
                    )} />

                    <Route exact path='/schedule/:studentId/update/:id' render={props => (
                        <ScheduleUpdate {...props} updateSchedule={this.updateSchedule} />
                    )} />
                </div>
            </div>
        );
    }
}

export default Schedule;