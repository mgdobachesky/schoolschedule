import { Component } from 'react';

import ScheduleCreateJSX from './CreateForm.jsx';
import './CreateForm.css';

class ScheduleCreate extends Component {

    constructor(props) {
        super(props);
        this.state = {quarter: '', courses: [], student: ''};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleMultiSelectValues = this.handleMultiSelectValues.bind(this);
        this.getCourseIds = this.getCourseIds.bind(this);
    }

    handleChange(event) {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'select-multiple' ? this.handleMultiSelectValues(target) : target.value;
        this.setState({[name]: value});
    }

    getCourseIds(courses) {
        if(courses) {
            return courses.map(c => c._id);
        }
        return [];
    }

    handleMultiSelectValues(target) {
        let multiSelectValues = this.state.courses;
        let existingCourseIds = this.getCourseIds(this.state.courses)
        if(existingCourseIds.includes(target.value)) {
            multiSelectValues = multiSelectValues.filter(msv => msv._id !== target.value);
        } else {
            multiSelectValues.push({'_id': target.value})
        }
        return multiSelectValues;
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.createSchedule(this.state);
    }

    componentDidMount() {
        let schedule = this.state;
        schedule.student = this.props.match.params.studentId;
        this.setState(schedule);
    }

    render() {
        return (ScheduleCreateJSX.call(this));
    }
}

export default ScheduleCreate;