import React from 'react';
import { Link } from 'react-router-dom';

function StudentRow(props) {
    return props.students.map((student, index) => {
        return(
            <tr key={student._id}>
                <td>{student.name}</td>
                <td>
                    <Link to={"/schedule/"+student._id} className="btn btn-primary">Schedules</Link>
                </td>
                <td>
                    <Link to={"/student/update/"+student._id} className="btn btn-primary">Edit</Link>
                </td>
                <td>
                    <button className="btn btn-danger" onClick={() => props.deleteStudent(student)}>Delete</button>
                </td>
            </tr>
        );
    });
}

let StudentTableJSX = function() {
    return (
        <div className="m-2 flex-grow-1">
            <table className="table table-striped">
                <thead className="thead-dark">
                    <tr>
                        <th colSpan="4">Students</th>
                    </tr>
                </thead>
                <tbody>
                    <StudentRow students={this.props.students} deleteStudent={this.props.deleteStudent} />
                </tbody>
            </table>
        </div>
    )
}

export default StudentTableJSX;