const scheduleService = require('../services/schedule');

function sendJsonResponse(res, status, content) {
    res.status(status);
    res.json(content);
}

async function createSchedule(req, res) {
    try {
        let results = await scheduleService.create(req.body);
        sendJsonResponse(res, 200, results);
    } catch (error) {
        sendJsonResponse(res, 404, error);
    }
}

async function readSchedule(req, res) {
    try {
        let results = await scheduleService.read(req.params.scheduleId);
        sendJsonResponse(res, 200, results);
    } catch (error) {
        sendJsonResponse(res, 404, error);
    }
}

async function readAllSchedules(req, res) {
    try {
        let results = await scheduleService.readAll(req.params.studentId);
        sendJsonResponse(res, 200, results);
    } catch (error) {
        sendJsonResponse(res, 404, error);
    }
}

async function updateSchedule(req, res) {
    try {
        let results = await scheduleService.update(req.body);
        sendJsonResponse(res, 200, results);
    } catch (error) {
        sendJsonResponse(res, 404, error);
    }
}

async function deleteSchedule(req, res) {
    try {
        let results = await scheduleService.delete(req.body);
        sendJsonResponse(res, 200, results);
    } catch (error) {
        sendJsonResponse(res, 404, error);
    }
}

module.exports = {
    create: createSchedule,
    read: readSchedule,
    readAll: readAllSchedules,
    update: updateSchedule,
    delete: deleteSchedule
}