import { Component } from 'react';

import CourseService from '../../../service/CourseService';
import CourseUpdateJSX from './UpdateForm.jsx';
import './UpdateForm.css';

class CourseUpdate extends Component {

    constructor(props) {
        super(props);
        this.state = {name: ''}

        this.courseService = new CourseService();

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.courseService.getCourse(this.props.match.params)
        .then(res => {
            this.setState(res.data);
        }).catch(err => console.log(err));
    }

    handleChange(event) {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({[name]: value});
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.updateCourse(this.state);
    }

    render() {
        return (CourseUpdateJSX.call(this));
    }
}

export default CourseUpdate;