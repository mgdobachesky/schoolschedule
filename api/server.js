#!/usr/bin/env node

const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');

require('./models/db')
const db = mongoose.connections;
const studentRoutes = require('./routes/student');
const scheduleRoutes = require('./routes/schedule');
const courseRoutes = require('./routes/course');

const api = express();
api.use(express.static('public'));
api.use(cors());
api.use(bodyParser.urlencoded({extended: true}));
api.use(bodyParser.json());

api.use('/student', studentRoutes);
api.use('/schedule', scheduleRoutes);
api.use('/course', courseRoutes);

api.use((req, res, next) => {
    const error = new Error('Not Found');
    error.status = 404;
    next(error);
});

api.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        "error": {
            "message": error.message,
            "status": error.status
        }
    });
});

api.set('port', process.env.PORT || 3001);
const server = api.listen(api.get('port'), () => {
    console.log('API started on port ' + server.address().port)
})


