import { Component } from 'react';

import ScheduleService from '../../../service/ScheduleService';
import ScheduleUpdateJSX from './UpdateForm.jsx';
import './UpdateForm.css';

class ScheduleUpdate extends Component {

    constructor(props) {
        super(props);
        this.state = {quarter: '', courses: []}

        this.scheduleService = new ScheduleService();

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleMultiSelectValues = this.handleMultiSelectValues.bind(this);
        this.getCourseIds = this.getCourseIds.bind(this);
    }

    componentDidMount() {
        this.scheduleService.getSchedule(this.props.match.params)
        .then(res => {
            this.setState(res.data);
        }).catch(err => console.log(err));
    }

    handleChange(event) {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'select-multiple' ? this.handleMultiSelectValues(target) : target.value;
        this.setState({[name]: value});
    }

    getCourseIds(courses) {
        if(courses) {
            return courses.map(c => c._id);
        }
        return [];
    }

    handleMultiSelectValues(target) {
        let multiSelectValues = this.state.courses;
        let existingCourseIds = this.getCourseIds(this.state.courses)
        if(existingCourseIds.includes(target.value)) {
            multiSelectValues = multiSelectValues.filter(msv => msv._id !== target.value);
        } else {
            multiSelectValues.push({'_id': target.value})
        }
        return multiSelectValues;
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.updateSchedule(this.state);
    }

    render() {
        return (ScheduleUpdateJSX.call(this));
    }
}

export default ScheduleUpdate;