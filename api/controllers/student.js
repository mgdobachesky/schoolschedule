const studentService = require('../services/student');

function sendJsonResponse(res, status, content) {
    res.status(status);
    res.json(content);
}

async function createStudent(req, res) {
    try {
        let results = await studentService.create(req.body);
        sendJsonResponse(res, 200, results);
    } catch (error) {
        sendJsonResponse(res, 404, error);
    }
}

async function readStudent(req, res) {
    try {
        let results = await studentService.read(req.params.studentId);
        sendJsonResponse(res, 200, results);
    } catch (error) {
        sendJsonResponse(res, 404, error);
    }
}

async function readAllStudents(req, res) {
    try {
        let results = await studentService.readAll();
        sendJsonResponse(res, 200, results);
    } catch (error) {
        sendJsonResponse(res, 404, error);
    }
}

async function updateStudent(req, res) {
    try {
        let results = await studentService.update(req.body);
        sendJsonResponse(res, 200, results);
    } catch (error) {
        sendJsonResponse(res, 404, error);
    }
}

async function deleteStudent(req, res) {
    try {
        let results = await studentService.delete(req.body);
        sendJsonResponse(res, 200, results);
    } catch (error) {
        sendJsonResponse(res, 404, error);
    }
}

module.exports = {
    create: createStudent,
    read: readStudent,
    readAll: readAllStudents,
    update: updateStudent,
    delete: deleteStudent
}